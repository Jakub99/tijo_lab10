package pl.edu.pwsztar.service;

import pl.edu.pwsztar.domain.dto.Point2D;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;

public interface ConvertService {

    Point2D convertToXY(String input);
}
