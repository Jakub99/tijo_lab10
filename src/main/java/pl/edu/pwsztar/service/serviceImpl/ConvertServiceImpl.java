package pl.edu.pwsztar.service.serviceImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.controller.ChessApiController;
import pl.edu.pwsztar.domain.dto.Point2D;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.service.ConvertService;

@Service
public class ConvertServiceImpl implements ConvertService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ChessApiController.class);

    @Autowired
    public ConvertServiceImpl(){}

    @Override
    public Point2D convertToXY(String input) {

        Point2D point2D = getPosition(input);

        return point2D;
    }

    private Point2D getPosition(String str) {
        str = str.replace("_", "");
        char[] c = str.toCharArray();

        Point2D point2D = new Point2D(c[0] - 'a' + 1, Character.getNumericValue(c[1]));

        return point2D;
    }

}
