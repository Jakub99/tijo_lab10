package pl.edu.pwsztar.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.domain.dto.Point2D;
import pl.edu.pwsztar.service.ConvertService;

@Controller
@RequestMapping(value="/api")
public class ChessApiController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ChessApiController.class);

    private final ConvertService convertService;

    @Autowired
    public ChessApiController(ConvertService convertService) {
        this.convertService = convertService;
    }

    @CrossOrigin
    @PostMapping(value = "/chess/is-correct-move")
    public ResponseEntity<Boolean> isCorrectMove(@RequestBody FigureMoveDto figureMoveDto) {
        LOGGER.info("*** move details : {}", figureMoveDto);

        Point2D source = convertService.convertToXY(figureMoveDto.getSource());
        Point2D destination = convertService.convertToXY(figureMoveDto.getDestination());

        Boolean valid = Math.abs(source.getX() - destination.getX()) == Math.abs(source.getY() - destination.getY());

        return ResponseEntity.ok(valid);
    }
}
